﻿using System;

namespace exercise_18
{
    class Program
    {
        static void Main(string[] args)
        {
            var num1=9;
            var num2=8;
            var num3=7;
            var num4=6;
            var num5=5;
            var num6=4;
           
            /*
            a)______________________________________________
            Console.WriteLine($"The value of {num1},"); 
            Console.WriteLine($"The value of {num2}");
            Console.WriteLine($"The value of {num3}"); 
            Console.WriteLine($"The value of {num4}");
            Console.WriteLine($"The value of {num5}");
            ______________________________________________*/
            /* 
            B}_____________________________________________
            Console.WriteLine($"The value of {num1+num2}");
            Console.WriteLine($"The value of {num3+num4}");
            Console.WriteLine($"The value of {num5+num4}");
            ______________________________________________
            */
            /*
            C)_____________________________________________
            Console.WriteLine($"The Total sum is ={num1+=num1+num2+num3+num4}");
             */
            
        }
    }
}
